obj-m:= i2c_flash.o

ARCH=x86
CROSS_COMPILE=i586-poky-linux-

all:
	make -C /opt/clanton-full/1.4.2/sysroots/i586-poky-linux/usr/src/kernel M=$(PWD) modules
clean:
	rm -f *.ko
	rm -f *.o
	rm -f Module.symvers
	rm -f modules.order
	rm -f *.mod.c
