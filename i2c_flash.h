#ifndef __I2C_FLASH_H__
#define __I2C_FLASH_H__

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/workqueue.h>

#define I2C_FLASH_MAGIC 'k'
#define DEVICE_NAME "i2c_flash"
#define I2C_MUX 29
#define I2C_LED 26
#define EEPROM_PAGE_SIZE 64
#define EEPROM_SIZE 512
#define EEPROM_I2C_ADDR 0x54

#define FLASHGETS	_IOR(I2C_FLASH_MAGIC, 1, int)
#define FLASHGETP	_IOR(I2C_FLASH_MAGIC, 2, int)
#define FLASHSETP	_IOW(I2C_FLASH_MAGIC, 3, int)
#define FLASHERASE      _IO(I2C_FLASH_MAGIC, 4)

static struct class *i2c_flash_class = NULL;
static dev_t i2c_flash_major_number;
static dev_t i2c_flash_minor_number = 0;

static struct i2c_board_info i2c_flash_info = {
	I2C_BOARD_INFO(DEVICE_NAME, 0x54),
};

typedef struct {
	struct work_struct work;
	char *usr_buf;
	int count;
}i2c_flash_work_struct;
		
struct i2c_flash_dev{
	char name[20];
	dev_t devno;
	struct cdev cdev;
	struct semaphore sem;
	struct device *dev;
	struct i2c_adapter *i2c_flash_adapter;
	struct i2c_client *i2c_flash_client;
	struct workqueue_struct *i2c_flash_workqueue;
	int page_nr;
} *i2c_flash_devp;

int __init i2c_flash_init(void);
void __exit i2c_flash_exit(void);
static int i2c_flash_open(struct inode *inode, struct file *file);
static int i2c_flash_release(struct inode *inode, struct file *file);
ssize_t i2c_flash_write(struct file *file, const char *buf, size_t count, loff_t *ppos);
ssize_t i2c_flash_read(struct file *file, char *buf, size_t count, loff_t *ppos);
long i2c_flash_ioctl(struct file *file, unsigned int cmd, unsigned long arg); 
#endif
