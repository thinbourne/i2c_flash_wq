#include "i2c_flash.h"

static struct file_operations i2c_flash_fops = {
	.owner		= THIS_MODULE,
	.open		= i2c_flash_open,
	.release	= i2c_flash_release,
	.write 		= i2c_flash_write,
	.read		= i2c_flash_read,
	.unlocked_ioctl = i2c_flash_ioctl,
};

long i2c_flash_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){
	int page_number, ret, i;
	char *erase_buf;
	struct i2c_msg i2c_erase_msg;
	printk("INSIDE I2C FLASH IOCTL\n");
	switch(cmd){
		case FLASHGETS: printk("GET I2C FLASH STATUS\n");
				put_user(0, (int*)arg);
				break;
		case FLASHGETP: printk("GET I2C FLASH POINTER\n");
				printk("PAGE NR:%d", i2c_flash_devp->page_nr);
				page_number = (unsigned long)i2c_flash_devp->page_nr;
				ret = copy_to_user((void *)arg, &page_number, sizeof(int));
				if(ret < 0){
					return -1;
				}
				break;
		case FLASHSETP: printk("SET I2C FLASH PAGE POINTER\n");
				page_number = (int)arg;
				if(page_number > 511){
					printk("INVALID PAGE NUMBER\n");
					return -1;
				}
				i2c_flash_devp->page_nr = page_number;
				printk("PAGE NUMBER:%d", i2c_flash_devp->page_nr);
				break;
		case FLASHERASE:printk("ERASING EEPROM\n");
				erase_buf = kmalloc(EEPROM_PAGE_SIZE * EEPROM_SIZE, GFP_KERNEL);
				memset(erase_buf, 1, EEPROM_PAGE_SIZE * EEPROM_SIZE);
				page_number = 0;
				i2c_erase_msg.addr  = EEPROM_I2C_ADDR;
				i2c_erase_msg.len   = EEPROM_PAGE_SIZE * EEPROM_SIZE;
				i2c_erase_msg.flags = 0;
				erase_buf[1] = (unsigned char)(page_number & 0xff);
				i &= 0xff00;
				erase_buf[0] = (unsigned char)((page_number >> 8) & 0x7f);
				i2c_erase_msg.buf   = erase_buf;

				ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_erase_msg, 1);
				if(ret < 0){
					printk("ERROR IN ERASING EEPROM\n");
					return -1;
				}
				kfree(erase_buf);
				break;
		default: printk("INVALID I2C FLASH COMMAND\n");
			 return -EINVAL;
	}
	return 0;
}

static void my_work_write(struct work_struct *work){
	int page_number, ret, size;
	struct i2c_msg i2c_msg;
	int count;
	i2c_flash_work_struct *i2c_work_struct = (i2c_flash_work_struct*)work;

	printk("Inside WORK QUEUE WRITE\n");
	page_number = i2c_flash_devp->page_nr;
	page_number *= 64;
	size = EEPROM_PAGE_SIZE; 

	i2c_work_struct->usr_buf[1] = (unsigned char)(page_number & 0xff);
	page_number &= 0xff00;
	i2c_work_struct->usr_buf[0] = (unsigned char)((page_number >> 8) & 0x7f);
	count = i2c_work_struct->count;

	i2c_msg.addr  = EEPROM_I2C_ADDR;
	i2c_msg.flags = 0;
	i2c_msg.len   = EEPROM_PAGE_SIZE;
	i2c_msg.buf   = i2c_work_struct->usr_buf;

	//gpio_set_value_cansleep(I2C_LED, 1);
	ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_msg, 1);
	//gpio_set_value_cansleep(I2C_LED, 0);
	if(ret < 0){
		printk("I2C TRANSFER ERROR:%d\n", ret);
	}
	printk("WRITE SUCCESS\n");
}
ssize_t i2c_flash_write(struct file *file, const char *buf,
		size_t count, loff_t *ppos){
	int copied, ret, size;
	char *usr_buf;
	i2c_flash_work_struct *write_work;

	size = count * 64;
	usr_buf = kmalloc(size, GFP_KERNEL);
	printk("SIZE OF BUF:%d\n", size);
	copied = copy_from_user(usr_buf, buf, size);
	if(copied == 0){
		printk("SUCCESSFULLY COPIED\n");
		printk("USER MESSAGE: %s\n", usr_buf);
	}
	else{
		printk("USR MSG COPY ERROR\n");
		return -ENOMEM;
	}

	write_work = (i2c_flash_work_struct*)kmalloc(sizeof(i2c_flash_work_struct), GFP_KERNEL);
	if(write_work){
		INIT_WORK((struct work_struct *)write_work, my_work_write);
		write_work->usr_buf 	= usr_buf;
		write_work->count	= count;
		ret = queue_work(i2c_flash_devp->i2c_flash_workqueue, (struct work_struct*)write_work);
		if(ret != 0){
			printk("WRITE WORK QUEUED SUCCESSFULLY\n");
		}
		else{
			printk("WRITE WORK QUEUE ADD ERROR\n");
			return -1;
		}
	}
	else{
		printk("WRITE WORK NO MEMORY\n");
		return -1;
	}
	/*i2c_msg.addr  = EEPROM_I2C_ADDR;
	i2c_msg.flags = 0;
	i2c_msg.len   = count * 64;
	i2c_msg.buf   = usr_buf;

	printk("USR_BUF BS:%s", usr_buf);
	gpio_set_value_cansleep(I2C_LED, 1);
	ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_msg, 1);
	gpio_set_value_cansleep(I2C_LED, 0);
	if(ret < 0){
		printk("I2C TRANSFER ERROR:%d\n", ret);
		return -1;
	}
	kfree(usr_buf);*/
	return 0;
}

ssize_t i2c_flash_read(struct file *file, char *buf,
		size_t count, loff_t *ppos){
	int ret, bytes, page_number;
	char *usr_buf, tmp_buf[2];	
	struct i2c_msg i2c_msg[2];

	usr_buf = kmalloc(count * 64, GFP_KERNEL);
	page_number = i2c_flash_devp->page_nr;
	printk("PAGE NUMBER:%d", page_number);
	page_number *= 64;
	tmp_buf[1] = (unsigned char)(page_number & 0xff);
	page_number &= 0xff00;
	tmp_buf[0] = (unsigned char)((page_number >> 8) & 0x7f);

	i2c_msg[0].addr	    = EEPROM_I2C_ADDR;
	i2c_msg[0].flags    = 0;
	i2c_msg[0].len      = sizeof(tmp_buf);
	i2c_msg[0].buf      = tmp_buf;

	i2c_msg[1].addr     = EEPROM_I2C_ADDR;
	i2c_msg[1].flags    = I2C_M_RD;
	i2c_msg[1].len      = count * 64;
	i2c_msg[1].buf      = usr_buf;

	gpio_set_value_cansleep(I2C_LED, 1);
	ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, i2c_msg, 2);
	gpio_set_value_cansleep(I2C_LED, 0);
	
	if(ret < 0){
		printk("I2C TRANSFER ERROR\n");
		return -1;
	}
	bytes = copy_to_user(buf, usr_buf, count * 64); 
	if(bytes == 0){
		printk("SUCCESS\n");
	}
	else{
		printk("COPY TO USER ERROR\n");
	}
	return 0;
}
		
static int i2c_flash_open(struct inode *inode, struct file *file){
	return 0;
}

static int i2c_flash_release(struct inode *inode, struct file *file){
	return 0;
}

int __init i2c_flash_init(void){
	int ret;
	int error, device_num;
	struct i2c_adapter *i2c_flash_adapter;
	struct i2c_client *i2c_flash_client;

	if(alloc_chrdev_region(&i2c_flash_major_number, 0, 1, DEVICE_NAME) < 0){
		printk(KERN_DEBUG "Can't register device\n");
		return -1;
	}

	i2c_flash_class = class_create(THIS_MODULE, DEVICE_NAME);
	device_num = MKDEV(MAJOR(i2c_flash_major_number), i2c_flash_minor_number);
	i2c_flash_devp = kmalloc(sizeof(struct i2c_flash_dev), GFP_KERNEL);
	if(!i2c_flash_devp){
		printk("I2C FLASH DEVP ERROR\n");
		return -ENOMEM;
	}

	cdev_init(&i2c_flash_devp->cdev, &i2c_flash_fops);
	i2c_flash_devp->cdev.owner	= THIS_MODULE;
	i2c_flash_devp->devno	 	= device_num;
	error = cdev_add(&i2c_flash_devp->cdev, device_num, 1);
	if(error){
		printk("CDEV ADD ERROR\n");
		return -1;
	}

	i2c_flash_devp->dev = device_create(i2c_flash_class, NULL, device_num, NULL, "i2c_flash");

	ret = gpio_request_one(I2C_MUX, GPIOF_OUT_INIT_LOW, "i2c_flash_pin");
	if(ret != 0){
		printk("GPIO EXPORT ERROR\n");
		return -1;
	}	

	gpio_set_value_cansleep(I2C_MUX, 0);

	/*ret = gpio_request_one(I2C_LED, GPIOF_OUT_INIT_LOW, "i2c_led");
	if(ret != 0){
		printk("GPIO IO 8 ERROR\n");
		return -1;
	}
	gpio_set_value_cansleep(I2C_LED, 0);*/

	i2c_flash_adapter = i2c_get_adapter(0);
	if(!i2c_flash_adapter){
		printk("I2C GET ADAPTER ERROR\n");
		return -1;
	}
	i2c_flash_devp->i2c_flash_adapter = i2c_flash_adapter;

	i2c_flash_client = i2c_new_device(i2c_flash_adapter, &i2c_flash_info);
	if(!i2c_flash_client){
		printk("I2C FLASH CLIENT NEW DEVICE ERROR\n");
		i2c_put_adapter(i2c_flash_adapter);
		return -1;
	}

	i2c_flash_devp->i2c_flash_client = i2c_flash_client;	
	i2c_flash_devp->page_nr = 0;
	i2c_flash_devp->i2c_flash_workqueue = create_workqueue("i2c_flash_workqueue");
	if(!i2c_flash_devp->i2c_flash_workqueue){
		printk("CANNOT CREATE I2C FLASH WORKQUEUE\n");
		return -1;
	}
	printk("I2C FLASH DRIVER INITIALIZED\n");	
	return 0;
}

void __exit i2c_flash_exit(void){
	gpio_free(I2C_MUX);
	gpio_free(I2C_LED);
	cdev_del(&i2c_flash_devp->cdev);

	i2c_unregister_device(i2c_flash_devp->i2c_flash_client);
	i2c_put_adapter(i2c_flash_devp->i2c_flash_adapter);
	device_destroy(i2c_flash_class, i2c_flash_devp->devno);
	unregister_chrdev_region((i2c_flash_major_number), 1);

	kfree(i2c_flash_devp);
	
	class_destroy(i2c_flash_class);
	printk("\nI2C FLASH DRIVER REMOVED");
}

module_init(i2c_flash_init);
module_exit(i2c_flash_exit);
MODULE_LICENSE("GPL v2");
